## Juan Diego Balanta
## id: 0220859
## 24 de Septiembre de 2020
##
## Como miembro de la comunidad académica de la Pontificia Universidad Javeriana Cali me comprometo
## a seguir los más altos estándares de integridad académica.
## Inspiración y Guia
## Notas de clase, codigo de cubrimiento de intervalos

from sys import stdin


def mic_wf(L,H,a):
  """
  Codigo de las notas de clase
  Cubrimiento de intervalos
  """
  a.sort()
  ans,low,n,ok,N = list(),L,0,True,len(a)
  cnt=len(a)
  while ok and low<H and n!=N:
    ok = a[n][0]<=low
    best,n = n,n+1
    while ok and n!=N and a[n][0]<=low:
      if a[n][1]>a[best][1]:
        best = n
      n += 1
    ans.append(best)
    cnt-=1
    low = a[best][1]
  ok = ok and low>=H
  if ok==False:
    ans = list()
  if ans==[]:cnt=-1
  return cnt

# def solve(L,arreglo):
#   print("El valor de L es:",L,"El valor del arreglo es:",arreglo)

def solve(L,arreglo):
    candidates=[]
    for i in range(len(arreglo)):
      #intervalo cerrado de influencia [x-r, x+r]
      lo=arreglo[i][0]-arreglo[i][1]
      hi=arreglo[i][0]+arreglo[i][1]
      candidates.append((lo,hi))
    ans=mic_wf(0,L,candidates)
    return ans


def main():
    tmp=True
    while tmp:
      
      L,G=map(int, stdin.readline().split())
      #print(L,G)
      if not (L==0 and G==0):
        arr=[]
        for i in range(G):
            pos,r=map(int, stdin.readline().split())
            arr.append((pos,r))
        print(solve(L,arr))
      else:
        tmp=False           
main()