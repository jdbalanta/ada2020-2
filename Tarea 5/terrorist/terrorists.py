## Juan Diego Balanta Posso
## 14 de Noviembre de 2020
## id: 0220859
## Como miembro de la comunidad académica de la Pontificia Universidad Javeriana Cali me comprometo
## a seguir los más altos estándares de integridad académica.
from sys import stdin
from math import *

primos ={2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101}
cache={}
def divisors(number):
    global primos
    if number==1:
        return 1
    #numDivs es el numero de divisores encontrados hasta el momento    
    numDivs,upperLimitNumber=1,ceil(sqrt(number))

    #print("la raiz cuadrada de:",number,"es", upperLimitNumber)
    for primo in primos:
        if primo*primo>number:
            break
        exponents=0
        while(number%primo==0):
            #print(number)
            number//=primo
            exponents+=1
        numDivs*=(exponents+1)
    #print(numDivs)
    return numDivs*2 if number!=1 else numDivs

def solve(divisors):
    if divisors in primos:
        return True
    else:
        return False

def main():
    ##
    #   maxDivs representa el numero de divisores mas grandes hasta el momento
    #   maxFound es el valor al que esta ligado los maxDivs 
    ##
    numCases=int(stdin.readline())
    for i in range(numCases):
        L,U=[int(x) for x in stdin.readline().split()]
        ans=[]
        for i in range(L,U+1):
            if i in cache:
                factors=cache[i]
            else:
                factors=divisors(i)
                cache[i]=factors
            if solve(factors):
                ans.append(i)
        if len(ans)==0:
            print("-1")
        else:
            print(" ".join([str(x) for x in ans]))

    
main()