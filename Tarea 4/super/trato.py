from sys import stdin

MAX=31
preans = [[-1 for _ in range(MAX)] for _ in range(MAX)]

def solve(n,low,hi,pans):
    global preans
    if n==hi:
        preans[low][hi] = pans
    else:
        i=0
        while preans[low][hi]==-1 and i!=10:
            tmp = pans*10+i
            if tmp!=0:
                if n+1>=low:
                    if tmp%(n+1)==0: 
                        solve(n+1,low,hi,tmp)
                else:
                    solve(n+1,low,hi,tmp)
            i+=1
def precalc():
    for hi in range(1,MAX):
        for low in range(1,hi):
            solve(0,low,hi,0)

def main():
    precalc()
    # for x in preans:
    #     print(x)
    print("_____________________________________________El resultado es_____________________________________________")
    print(preans)
main()
