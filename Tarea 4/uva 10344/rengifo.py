from sys import stdin
​
def r(): return stdin.readline().strip()
​
​
def solve(A, ac, vis):
    ans = bool()
    if sum(vis)==5: ans = ac==23
    else:
        for i in range(5):
            if not vis[i]:
                vis[i]=1
                x=A[i]
                #Try with dif
                ans = solve(A, ac-x, vis)
                delta = x+ac-23
                #Try with sum
                if not ans and delta > 0 and (sum(A)>=delta):
                    ans = ans or solve(A, x+ac, vis)
                elif delta <= 0: ans = ans or solve(A, x+ac, vis)
                delta = x*ac - 23
                #Try with mult
                if not ans and delta > 0 and (sum(A)>=delta):
                    ans = ans or solve(A, x*ac, vis)
                elif delta <= 0: ans = ans or solve(A, x*ac, vis)
                vis[i]=0
    return ans
​
def all_eq(A): return len(set(A))==1
​
def main():
    while 1:
        A = list(map(int, r().split()))
        if all_eq(A) and A[0]!=23: print("Impossible")
        elif all_eq(A): print("Possible")
        else:
            if sum(A)==0: break
            ans = False
            vis = [0 for _ in range(5)]
            for i in range(5):
                vis[i]=1
                ac = A[i]
                ans = ans or solve(A, ac, vis)
                vis[i]=0
                if ans: break
            print("Possible" if ans else "Impossible")
​
​
main()
