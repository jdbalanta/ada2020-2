from sys import stdin
## Juan Diego Balanta Posso
## 26 de Agosto de 2020
##
## Como miembro de la comunidad académica de la Pontificia Universidad Javeriana Cali me comprometo
## a seguir los más altos estándares de integridad académica.


INF,EPS = float('-inf'),1e-9
def particionRouters(mitad,casas):
  temp=1
  rangoDeRouter=mitad*2
  posicion=0
  for i in range(len(casas)):
    if (casas[i]-casas[posicion])>rangoDeRouter:
     # print("la casa:", i, " está en el rango de:",rangoDeRouter )
      #print("DISTANCIA",casas[i]-casas[posicion])
      temp=temp+1
      posicion=i      
  return temp #cantidad de routers

def solve(n, m, house):
  ans = 0
  # place your code here!
  #print("El numero de routers es:",n)
  #print("El numero de casas es:",m)
  house.sort()
  #print("Las casas son es:",house)
  low=0
  hi=house[len(house)-1]-house[0]
  #print("low y hi son:",low,hi)
  while (hi-low>0.5):
    mitad=(hi+low)/2
    if particionRouters(mitad,house) <= n:
      hi=mitad
    else:
      low=mitad
  return mitad

def main():
  tc = int(stdin.readline())
  while tc!=0:
    n,m = map(int, stdin.readline().split())
    house = [ int(stdin.readline())*10 for _ in range(m) ]
    print('{0:.1f}'.format(solve(n, m, house)/10))
    tc -= 1

main()
